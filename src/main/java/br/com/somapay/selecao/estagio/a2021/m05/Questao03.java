package br.com.somapay.selecao.estagio.a2021.m05;

public class Questao03 {

	

	public Integer[] tribonacci(Integer[] sequenciaInicial, Integer tamanhoSequenciaFinal) {

		Integer[] tribNumb = new Integer[tamanhoSequenciaFinal];

		switch (tamanhoSequenciaFinal) {
		case 2:
			tribNumb[0] = sequenciaInicial[0];
			tribNumb[1] = sequenciaInicial[1];
			return tribNumb;
		case 1:
			tribNumb[0] = sequenciaInicial[0];
			return tribNumb;
		case 0:
			return tribNumb = new Integer[0];
		default:
			tribNumb[0] = sequenciaInicial[0];
			tribNumb[1] = sequenciaInicial[1];
			tribNumb[2] = sequenciaInicial[2];
			for (int i = 3; i < tamanhoSequenciaFinal; i++) {

				tribNumb[i] = tribNumb[i - 1] + tribNumb[i - 2] + tribNumb[i - 3];
			}
			return tribNumb;

		}
	}

	public static void main(String[] args) {
		Questao03 q3 = new Questao03();
		Integer[] a = q3.tribonacci(new Integer[] { 7, 9, 10 }, 5);

		for (int i = 0; i < a.length; i++) {
			System.out.println(a[i]);

		}

	}
}
