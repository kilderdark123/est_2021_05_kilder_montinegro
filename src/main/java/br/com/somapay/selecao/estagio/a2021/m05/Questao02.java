package br.com.somapay.selecao.estagio.a2021.m05;

public class Questao02 {

	public String mascarar(String dado) {
		
		
		
		int ultimo = dado.length();
		try {
			int quarto = dado.length() - 4;
			String result = new String(new char[quarto]).replace("\0", "#");
			return result + dado.substring(quarto, ultimo);

		} catch (Exception e) {
			return dado;

		}

	}

	public static void main(String[] args) {
		Questao02 q2 = new Questao02();

		System.out.println(q2.mascarar("12345678"));
	}
}
