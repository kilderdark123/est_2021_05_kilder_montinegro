package br.com.somapay.selecao.estagio.a2021.m05;

public class Questao01 {

	public Integer proximoQuadradoPerfeito(Integer numero)  {
		
		if(numero == null) return null;
		Double d = Math.sqrt(numero);
		Integer raizQuadrada = d.intValue();
		return d.intValue() == d.doubleValue() ? (raizQuadrada + 1) * (raizQuadrada + 1) : -1;
	}

	public static void main(String[] args) throws Exception {
		Questao01 q1 = new Questao01();

		System.out.println(q1.proximoQuadradoPerfeito(90));
	}
}
